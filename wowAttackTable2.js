$(function () {
    var data = {
        attack : {
            type: "melee",
            swing_type: "auto",
            name: "",
            wield: "single",
            from_behind: false
        },
        weapon: {
            min_dmg: 704,
            max_dmg: 1057,
            average_dmg: (704+1057)/2,
            speed: 3.6,
            final_multiplier: 1
        },
        attacker : {
            level: 80,
            creature_type: "pj",
            attack_power: 586
        },
        defender : {
            level: 80,
            creature_type: "pj",
            has_parry: true,
            has_shield: true
        },
        items : [{
               "tipo": "head",
               "strength": 128
            },
            {
                "tipo": "neck",
                "attack_power": 181
            },
            {
                "tipo": "shoulder",
                "strength": 99
            },
            {
                "tipo": "back",
                "attack_power": 181
            },
            {
                "tipo": "chest",
                "strength": 145
            },
            {
                "tipo": "wrist",
                "strength": 90
            },
            {
                "tipo": "hands",
                "strength": 99
            },
            {
                "tipo": "waist",
                "strength": 113
            },
            {
                "tipo": "legs",
                "strength": 125
            },
            {
                "tipo": "feet",
                "strength": 113
            },
            {
                "tipo": "set",
                "attack_power": 200
            },
            {
                "tipo": "finger_1",
                "strength": 69
            },
            {
            "tipo": "finger_2",
            "attack_power": 181
            },
            {
                "tipo": "ranged",
                "strength": 39
            },
            { 
                "tipo": "melee",
                "strength": 40,
                "attack_power": 224
            } ]
    };
    for (var i in data) {
        if (i !== "items") {
            for (var j in data[i]) {
                var current = $("div#" + i + " ." + j);
                if (current.attr("type") === "checkbox") {
                    current[0].checked = data[i][j];
                } else {
                    current[0].value = data[i][j];
                }
            }
        } else {
            var textarea = $("textarea")[0];
            textarea.value = JSON.stringify(data[i], undefined, 2);
        }
    }
    var chances = [
        {
            label: "miss",
            value: 5
        },
        {
            label: "dodge",
            value: 5
        },
        {
            label: "parry",
            value: 5
        },
        {
            label: "glanging blow",
            value: 10
        },
        {
            label: "block",
            value: 5
        },
        {
            label: "crit",
            value: 5
        },
        {
            label: "crushing blow",
            value: 0
        },
        {
            label: "normal",
            value: 90
        }
    ];
    recalculate();
    $("input, select").on("change click keyup", function(){
        var current = $(this);
        if (current.attr("type") === "checkbox") {
            data[current.parent().attr("id")][current.attr("class")] = this.checked;
        } else {
            data[current.parent().attr("id")][current.attr("class")] = parseFloat(this.value) ||
                this.value;
        }
        recalculate();
    });
    $("textarea").on("change", function(){
        var sum = 0;
        try {
            var obj = data.items = JSON.parse(this.value);
            obj.forEach(function(e){
                sum += (e.attack_power || 0) + (e.strength || 0) * 2;
            });
        } catch(a) {"";}
        data.attacker.extra_attack_power = sum;
        $(".extra_attack_power")[0].value = sum;
    }).change();
    function recalculate() {
        with (data) {
            // Miss
            var chance;
            if (attack.wield == "dual" && attack.swing_type == "auto") {
                chance = 24;
            } else {
                chance = 5;
            }
            switch (defender.creature_type) {
            case "mob":
                if (defender.level - attacker.level  <= 2) {
                    chance += (defender.level - attacker.level) * 0.5;
                } else {
                    chance += 1 + (defender.level - attacker.level - 2 ) * 2;
                }
                break;
            case "pj":
                if (defender.level >= attacker.level) {
                    chance += (defender.level - attacker.level) * 0.2;
                } else {
                    chance += (defender.level - attacker.level) * 0.1;
                }
                break;
            }
            chances[0].value = chance;
            

            // Dodge TODO
            if ((attack.from_behind && defender.creature_type === "pj") ||
                attack.type === "not_melee")
                chance = 0;
            else
                chance = 5 + (defender.level - attacker.level) * 0.2;
            chances[1].value = chance;
            
            // Parry
            if (attack.type === "melee" && defender.has_parry && !attack.from_behind)
                chances[2].value = 5 + (defender.level - attacker.level) * 0.2;
            else
                chances[2].value = 0;

            // Glancing blow
            if (attack.swing_type === "auto" && defender.creature_type === "mob" &&
                defender.level >= attacker.level) {
                chances[3].value = 10 + (defender.level - attacker.level) * 5;
            } else {
                chances[3].value = 0;
            }

            // Block
            if (defender.has_shield)
                chances[4].value = 5 + (defender.level - attacker.level) * 0.2;
            else
                chances[4].value = 0;

            // Crit
            if (defender.creature_type === "mob")
                chances[5].value = 5 - (defender.level - attacker.level);
            else
                chances[5].value = 5 - (defender.level - attacker.level) * 0.2;

            // Crushing blow
            if (attacker.creature_type === "mob" && attacker.level >= defender.level + 4)
                chances[6].value = (attacker.level - defender.level) * 10 - 15;
            else
                chances[6].value = 0;


            // console.log(data, chances);
            var sum = 0;
            for (var i = 0; i < chances.length - 1; i++) {
                sum += chances[i].value;
            }
            chances[chances.length - 1].value = 100 - sum;
            $("#miss").html(pprint(chance));
            
            // Graph
            nv.addGraph(function() {
              var chart = nv.models.pieChart()
                  .x(function(d) { return d.label })
                  .y(function(d) { return d.value })
                  .showLabels(true);

                d3.select("#chart")
                    .datum(chances)
                  .transition().duration(1200)
                    .call(chart);

              return chart;
            });
        }
        damage();
    }
    function pprint(num) {
        return num.toPrecision(num >= 10 ? 4 : 3);
    }
    var canvas = $("#dmg")[0];
    var ctx = canvas.getContext("2d");
    ctx.translate(0, canvas.height);
    ctx.scale(1, -1);
    ctx.scale(1, 0.06125);
    damage();
    function damage() {
        
        var final_average =
            (
                (data.weapon.max_dmg
                     +
                     data.weapon.speed
                         *
                         (data.attacker.attack_power
                              +
                              data.attacker.extra_attack_power)
                         / 14)
                + (data.weapon.min_dmg
                       + data.weapon.speed
                           *(data.attacker.attack_power
                               + data.attacker.extra_attack_power) / 14)) / 2;
        // console.log(final_average);

        var initial_average = (
                (data.weapon.max_dmg
                     +
                     data.weapon.speed
                         *
                         (588 + 548)
                         / 14)
                 +
                (
                    (data.weapon.min_dmg + data.weapon.speed * (588 + 548) / 14)
                )
            ) / 2;
        $("div#weapon .final_multiplier")[0].value = final_average / initial_average;
        $(".final_average")[0].value = final_average;
        var canvas = $("#dmg")[0];
        var ctx = canvas.getContext("2d");
        
        // Store the current transformation matrix
        ctx.save();

        // Use the identity matrix while clearing the canvas
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // Restore the transform
        ctx.restore();

        ctx.lineWidth = 2;
        ctx.strokeStyle = "red";
        ctx.fillStyle = "red";
        ctx.fillRect(6, data.weapon.min_dmg + data.weapon.speed * (588 + 548) / 14, 12, data.weapon.max_dmg - data.weapon.min_dmg);

        ctx.fillRect(18, (data.weapon.min_dmg + data.weapon.speed * (data.attacker.attack_power + data.attacker.extra_attack_power) / 14),
            12, (data.weapon.max_dmg - data.weapon.min_dmg));
    }
    
});
